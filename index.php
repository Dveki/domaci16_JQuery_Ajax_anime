<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Movie DB</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>

<div>
<button id="logovanje">Open Users</button>
<div class="login_form">
<?php
require("inc/header.php");		
?>
</div>
</div>
	<fieldset id="searchmovie">
		<legend>Search Movie</legend>
		<input id="search" type="text" />
	</fieldset>
	<script id="genres-template" type="x-tmpl-mustache">
		
		{{#genres}}
			<li class="link"><a href="#!{{.}}">{{.}}</a></li>
		{{/genres}}
		
	</script>

	<script id="movies-template" type="x-tmpl-mustache">	
		{{#movies}}
		<div class="moviesdispley" data-query="{{title}}">
		<div class="moviesdispley--id">{{id}}</div>
		<li class="link">{{title}}</li> 
		<img src="{{posterUrl}}" alt="poster" />	
		</div>
		{{/movies}}
	
		</script>

	<script id="movie-template" type="x-tmpl-mustache">
		
				{{#movies}}
				<button id="close-window">
						Close
				</button></br>
				<h2>Title:{{title}}</h2>
					<div id="image">
						<img src="{{posterUrl}}" alt="poster" />	
					</div>
					<p>Description: {{plot}}</p>
					<h3>Stars: {{actors}}</h3>
					<h3>Director: {{director}}</h3>
				{{/movies}}
				</script>

	<ul id="genres"></ul>
	<div id="movies"></div>
	<div id="movie"></div>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script src="https://unpkg.com/mustache@2.3.0/mustache.min.js"></script>
	<script src="index.js"></script>
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="//cdn.jsdelivr.net/npm/velocity-animate@2.0/velocity.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/velocity-animate@2.0/velocity.ui.min.js"></script>
	
	<script>
	var $$logovanje = $('.login_form');
	var $$input = $('input');

	$('#logovanje').on('click', function () {
	$$logovanje.addClass('pokazi');	
	 $$logovanje.velocity("fadeIn", { duration: 2500 });
			})
			
	</script>
	
	<script>
		App.start();
	</script>
</body>

</html>