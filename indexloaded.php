<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Movie DB</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>

<div>
<?php
require("inc/header.php");		
?>
</div>
</div>
	<fieldset id="searchmovie">
		<legend>Search Movie</legend>
		<input id="search" type="text" />
	</fieldset>
	<script id="genres-template" type="x-tmpl-mustache">
		
		{{#genres}}
			<li class="link"><a href="#!{{.}}">{{.}}</a></li>
		{{/genres}}
		
	</script>

	<script id="movies-template" type="x-tmpl-mustache">	
		{{#movies}}
		<div class="moviesdispley" data-query="{{title}}">
		<div class="moviesdispley--id">{{id}}</div>
		<div data-movie-id="{{id}}" class="favourite" data-faved="{{faved}}"></div>
		<li class="link">{{title}}</li> 
		<img src="{{posterUrl}}" alt="poster" />
			
		</div>
		{{/movies}}
	
		</script>

	<script id="movie-template" type="x-tmpl-mustache">
		
				{{#movies}}
				<button id="close-window">
						Close
				</button></br>
				<h2>Title:{{title}}</h2>
					<div id="image">
						<img src="{{posterUrl}}" alt="poster" />	
					</div>
					<p>Description: {{plot}}</p>
					<h3>Stars: {{actors}}</h3>
					<h3>Director: {{director}}</h3>
				{{/movies}}
				</script>

	<ul id="genres"></ul>
	<div id="movies"></div>
	<div id="movie"></div>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<script src="https://unpkg.com/mustache@2.3.0/mustache.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		var $$genreMovies = $('#genre_movies');
		var $$genres = $('#genres');
		var $$genresTpl = $('#genres-template').html();
		var $$searchmovie =$('#searchmovie');
		const $$search = $('#search');
		

        var $$movies = $('#movies');
 		var $$moviesTemplate = $('#movies-template').html();
		var $$favmovie = $('#favorite_movies');
		var $$movie = $('#movie');
		var $$movieTemplate = $('#movie-template').html();

//funkcija za otvaranje prozora koji prikazuje infoo izabranom filmu
function showMovieList($$filter){
	var $$moviesDispley = $('.moviesdispley');
$.each($$moviesDispley, function(i, value){
			var $$filtermovie = [];
			$$moviesDispley.eq(i).on('click', function () {
				$$filtermovie.push($$filter[i])
				//console.log($$filtermovies[i].title);
				//console.log($$moviesDispley.eq(i).attr('data-query'));
				//console.log($$filtermovie);
			//alert('radi'); 
			$$movie.addClass('pokazi');
			$$movie.html(Mustache.render($$movieTemplate, {
        movies: $$filtermovie
	  }))
	  $$movie.effect( "bounce", "slow" );
	  ;
	  $$filtermovie = [];

	  var $$closeWindow = $('#close-window');

$$closeWindow.on('click', function () {
	  $$movie.removeClass('pokazi');
});
	  });

	});
}
// KRAJ funkcije za otvaranje prozora koji prikazuje infoo izabranom filmu

//Funkcija za pretragu filmova
function searchMovie($$filtermovies) {
$$search.keyup(function(e) {
        //alert("radi");
        console.log(this.value);
        console.log($$filtermovies);
        const $$val = this.value;
        var $$query = "";
        var $moviesel = [];
		var $$moviesdispley = $('.moviesdispley');
		//console.log($$moviesdispley);
		$.each($$moviesdispley, function(i, value){
			$moviesel = $$moviesdispley[i];
			//console.log($$moviesel);
			$$query = $moviesel.getAttribute("data-query");
			console.log($$query);
			if ($$query.toLowerCase().indexOf($$val.toLowerCase()) === -1){
				$moviesel.style.display = "none";
			}
			 else {
            $moviesel.style.display = "inline-block";
          }
        });
		});
		}
		//Kraj funkcije

//Prikazivanje svih zanrova nakon clicka na dugme Genre
		$$genreMovies.on('click', function () {
			$.get('provera.php', {}, function (data) {
	   $$genresList = data.genres;
	   $$moviesList = data.movies;
	  // console.log($$genresList);
    doSomethingWithmovies($$genres)
}, 'json');
function doSomethingWithmovies(f) {
			//alert('radi');
			$$genres.html(Mustache.render($$genresTpl, {
			genres: $$genresList
	  }));
	  $$genres.toggle( "puff" );
	  $$genres.toggle( "slide" );
	  $$genreMovies.toggle( "explode" );
}
//console.log($$genres);

$$genres.on('click', function (e) {
	//console.log(e.target);
	var x = e.target.innerHTML;
	 // console.log(x);
	  var $$filtermovies = [];


 $.each($$moviesList, function( i, value ){
	$.each($$moviesList, function( j, value ) {
	if ($$moviesList[i].genres[j] == x)
			$$filtermovies.push($$moviesList[i])
		});
	});
		console.log($$filtermovies);
		
		$$searchmovie.addClass('pokazi');;
		$$movies.html(Mustache.render($$moviesTemplate, {
        movies: $$filtermovies
	  }));
	  $$movies.effect( "shake" );;
	  searchMovie($$filtermovies);
	  showMovieList($$filtermovies);

});


});










//Funkcije vezane za desavanja nakon clicka na Favorite Movie
		$$favmovie.on('click', function () {
       // alert('radi').
    $.get('provera.php', {}, function (data) {
       $$moviesList = data.movies;
    doSomethingWithmovies($$movies)
//console.log($movies);
}, 'json');

function doSomethingWithmovies(f)   {
    //console.log($moviesList);
    
    var $$favMovies = new Array();
// this will return an array with strings "1", "2", etc.
$$favMovies = $$favmovie.val().split(",");
    console.log($$favMovies);
     //NOW do something
     //console.log($moviesList.length);
     var $$filtermovies = [];
	
	 //used each instead of loop
	 $.each($$moviesList, function( i, value ){
		$.each($$favMovies, function( j, value ) {
			if ($$moviesList[i].id == $$favMovies[j])
			$$filtermovies.push($$moviesList[i])
		});
});
     /* for (var i = 0; i < $$moviesList.length; i++) {
        console.log($$moviesList[i].id);
        for (var j = 0; j <= $$favMovies.length; j++) {
          if ($$moviesList[i].id == $$favMovies[j]) {
            $$filtermovies.push($$moviesList[i]);
            //console.log($moviesList.length);
          }
        }
	  }*/
      //console.log(filtermovies.length);
      //console.log(x);
      console.log($$filtermovies);
      
      $$movies.html(Mustache.render($$moviesTemplate, {
        movies: $$filtermovies
	  }));
	  $$movies.effect( "shake" );
	
	  showMovieList($$filtermovies);

}
			});
			//Kraj za Favorite Movie
	
	</script>
	
	
</body>

</html>